# Simple CPTs Search

A plugin to extend the functionality of pages with custom post types (CPTs) such as Blog, News and Cases.



Example of shortcode ```[simple-search post-types="post, news"  element-count="2" ] ```


## 

You can choose the pagination type on the installed plugins page near the plugin activity control or use the ```pagination-type=""``` parameters with ```pagination``` or ```loadmore``` value.