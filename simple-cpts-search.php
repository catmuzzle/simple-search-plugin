<?php
/**
 * Simple CPTs Search
 *
 * @package       SIMPLECPTS
 * @author        Stanislav Лщыешгл
 * @version       1.0.0
 *
 * @wordpress-plugin
 * Plugin Name:   Simple CPTs Search
 * Plugin URI:
 * Description:   A plugin to extend the functionality of pages with custom post types (CPTs) such as Blog, News and Cases. Example of shortcode [simple-search post-types="post, news"  element-count="2" ] You can choose pagination on Installed plugins page or using pagination-type="" params with pagination or load more value.
 * Version:       1.0.0
 * Author:        Stanislav
 * Author URI:
 * Text Domain:   simple-cpts-search
 * Domain Path:   /languages
 */
namespace SimpleCptsSearch;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

//define plugin
define( 'SIMPLE_CPTS_SEARCH_NAME', 'simple-cpts-search' );
define( 'SIMPLE_CPTS_SEARCH_DIR', plugin_dir_url( __FILE__ ) );
define( 'SIMPLE_CPTS_SEARCH_PATH', plugin_dir_path(__FILE__) );
define( 'SIMPLE_CPTS_SEARCH_BASE_NAME', plugin_basename(__FILE__) );

/**
 * Autoload classes
 */
require_once dirname(__FILE__)."/vendor/autoload.php";



/**
 * Begins execution of the plugin.
 */
function run_simple_cpt_search() {

    $plugin = new SimpleCPTSearch();
    $plugin->run();

}
run_simple_cpt_search();