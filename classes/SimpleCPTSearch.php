<?php

namespace SimpleCptsSearch;

class SimpleCPTSearch
{
    /**
     * The unique identifier of this plugin.
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;
    protected $plugin_path;
    private $pagination_setting_name;
    private $nonce_title;


    public function __construct()
    {
        new SimpleCPTSettings();
        $this->plugin_name             = SIMPLE_CPTS_SEARCH_NAME;
        $this->plugin_path             = SIMPLE_CPTS_SEARCH_PATH;
        $this->nonce_title             = 'simple-search-nonce';
        $this->pagination_setting_name = 'simple-search-pagination-type';
        $this->pagination_type         = get_option($this->pagination_setting_name, null);
        //set default
        if (is_null($this->pagination_type)) {
            update_option($this->pagination_setting_name, 'pagination');
            $this->pagination_type = 'pagination';
        }

        add_action('wp_ajax_get_search_results_ajax', array($this, 'get_search_results_ajax'));
        add_action('wp_ajax_no_priv_get_search_results_ajax', array($this, 'get_search_results_ajax'));
        add_action('wp_enqueue_scripts', array($this, 'define_styles_scripts'));
    }

    public function run()
    {
        add_shortcode('simple-search', [$this, 'simple_search_add_shortcode']);
    }


    /**
     * Register the hooks related to the  functionality
     * of the plugin.
     */
    public function define_styles_scripts()
    {
        global $wp_query;
        wp_register_style($this->plugin_name, SIMPLE_CPTS_SEARCH_DIR . '/assets/css/simple-search-styles.css');
        wp_register_script(
            $this->plugin_name,
            SIMPLE_CPTS_SEARCH_DIR . '/assets/js/simple-search-main.js',
            array('jquery')
        );
        wp_localize_script(
            $this->plugin_name,
            'simple_search_ajax_obj',
            array(
                'ajax_url'        => admin_url('admin-ajax.php'),
                'nonce'           => wp_create_nonce($this->nonce_title),
                'pagination_type' => $this->pagination_type,
                'query_vars' => json_encode( $wp_query->query )
            )
        );
    }


    public function get_search_results_ajax()
    {
        check_ajax_referer($this->nonce_title, 'nonce');

        $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 1;
        $search_string = (isset($_POST['searchString'])) ? $_POST['searchString'] : '';
        $post_type = (isset($_POST['types'])) ? $_POST['types'] : '';
        $per_page = (isset($_POST['perPage'])) ? intval($_POST['perPage']) : 3;

        echo $this->fetch_results($page, $post_type ,$per_page, $search_string);
        wp_die();
    }

    private function fetch_results($page, $post_type, $per_page, $search_string)
    {
        $args  = array(
            'post_type'      => explode(',', $post_type),
            'posts_per_page' => $per_page,
            'post_status'    => 'publish',
            's'              => sanitize_text_field($search_string),
            'paged'          => $page,
        );
        $query = new \WP_Query($args);



        if ($query->have_posts()) {
            $html = '';
            ob_start();

            while ($query->have_posts()) {
                $query->the_post();
                ?>
                <div class="search-result-item">
                    <h1><?php echo get_the_title(); ?></h1>
                    <a href="<?php echo get_the_permalink(); ?>">Read more</a>
                </div>
                <?php
            }

            if ($this->pagination_type == 'pagination') { ?>
                <div class="pagination">
                    <?php
                    echo paginate_links(array(
                        'total'   => $query->max_num_pages,
                        'current' => $page,
                        'add_args'=> false,
                        'prev_next'=> false,
                        'base' => '',
                        'aria_current'=> 'location'
                    )); ?>
                </div>
            <?php
            }
            wp_reset_postdata();
            $html .= ob_get_clean();
            if($this->pagination_type == 'loadmore'){
                wp_send_json(array('html' => $html, 'cur_page' => $page, 'max_page' => $query->max_num_pages));
            }else{
                wp_send_json(array('html' => $html));
            }

        } else {
            // Return an empty string if no posts are found
            wp_send_json(array('err' => _e('No posts are found', SIMPLE_CPTS_SEARCH_NAME)));
        }
    }


    public function simple_search_add_shortcode($atts)
    {

        wp_enqueue_script($this->plugin_name);
        wp_enqueue_style($this->plugin_name);

        if ( ! isset($atts["post-types"]) || ! isset($atts["element-count"])) {
            return _e('Wrong shortcode`s attributes added or missed.');
        }


        if (isset($atts["pagination-type"])) {
            $type = sanitize_text_field($atts["pagination-type"]);
            update_option('simple-search-pagination-type', $type);
            $this->pagination_type = $type;
        }

        static $count = 1;
        $html = '';
        ob_start(); ?>

        <div
                class="simple-search"
                id="simple-search-<?php echo $count; ?>"
        >
         <?php
             include($this->plugin_path . 'templates/search-form.php');
             include($this->plugin_path . 'templates/search-results.php');
         ?>
        </div>;

        <?php
        $count++;

        $html .= ob_get_clean();
        return $html;
    }
}