<?php
/**
 * Settings
 */
namespace SimpleCptsSearch;

class SimpleCPTSettings{

    public function __construct()
    {
        add_filter('plugin_action_links_'.SIMPLE_CPTS_SEARCH_BASE_NAME, array($this, 'salcode_add_plugin_page_settings_link'));
        add_action('admin_enqueue_scripts', array($this, 'add_scripts'));
        add_action("wp_ajax_set_template", array($this, "set_template"));
    }

    public function salcode_add_plugin_page_settings_link( $links ) {
        $html = '';
        $template = get_option('simple-search-pagination-type');
        ob_start();

        ?>
            <form id="template_setting" action="#">
                <p>Choose template:</p><input type="radio" id="pagination" name="template" value="pagination" <?php if($template == 'pagination'){ echo 'checked'; }?>>
                <label for="pagination">Pagination</label><br>
                <input type="radio" id="loadmore" name="template" value="loadmore" <?php if($template == 'loadmore'){ echo 'checked'; }?>>
                <label for="loadmore">Load more button</label>
            </form>
        <?php

        $html .= ob_get_clean();
        $links[] = "{$html}";
        return $links;
    }

    public function  add_scripts($hook) {
        if ('plugins.php' !== $hook) {return;}
        wp_enqueue_script('simple-search-settings', SIMPLE_CPTS_SEARCH_DIR . '/assets/js/admin-scripts.js', array('jquery'), null, true);
    }


    public function set_template(){
        update_option('simple-search-pagination-type', $_POST['templateType'], true);
        wp_send_json('ok');
        wp_die();
    }
}


