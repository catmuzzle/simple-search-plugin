document.addEventListener("DOMContentLoaded", function () {
    const templates = document.getElementsByName('template')

    templates.forEach((el)=>{
        el.addEventListener('click', ()=>{
            let formdata = {
                action: 'set_template',
                templateType: el.value,
            }
            fetch(ajaxurl, {
                method: "POST",
                body: new URLSearchParams(formdata)
            })
                .then((response) => response.json())
                .then((data) => {
                    console.log(data)
                })
                .catch((error) => {
                    console.log(error);
                });
        })
    })
})

