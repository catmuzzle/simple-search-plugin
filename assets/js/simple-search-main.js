/**
 * Search from script
 */
document.addEventListener("DOMContentLoaded", function () {

    const form_el = document.querySelector('.simple-search-form');
    const content_container = document.querySelector('.search-result-container')
    const loadmore_btn = document.querySelector('.load-more-posts')

    let formdata = {
        action: 'get_search_results_ajax',
        nonce: simple_search_ajax_obj.nonce,
        searchString: document.getElementsByName("simple-search-query")[0].value,
        postTypes: form_el.dataset.types,
        perPage: form_el.dataset.perPage
    }

    form_el.addEventListener('submit', (e) => {

        if (form_el) {
            fetch(simple_search_ajax_obj.ajax_url, {
                method: "POST",
                body: new URLSearchParams(formdata)
            })
                .then((response) => response.json())
                .then((data) => {
                    if (data) {
                        content_container.innerHTML = data.html

                        if (data.hasOwnProperty('max_page')) {
                            if (data.max_page > data.cur_page) {
                                if (!("maxPage" in loadmore_btn.dataset)) {
                                    loadmore_btn.classList.add("active")
                                    loadmore_btn.dataset.maxPage = data.max_page
                                    loadmore_btn.dataset.curPage = data.cur_page
                                }
                            }
                        } else {
                            const pagination_link = form_el.parentElement.querySelectorAll('.pagination a.page-numbers')

                            if (pagination_link.length > 0) {
                                pagination_link.forEach(function (elem) {
                                    elem.addEventListener("click", (e) => {
                                        e.preventDefault()
                                        formdata.pageNumber = parseInt(elem.innerHTML)
                                        getNextPage(formdata);

                                    });
                                });
                            }
                        }
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
        e.preventDefault()
    })

    // Pagination
    const getNextPage = (formdata, page) => {

        fetch(simple_search_ajax_obj.ajax_url, {
            method: "POST",
            body: new URLSearchParams(formdata)
        })
            .then((response) => response.json())
            .then((data) => {
                if (data) {
                    content_container.innerHTML = data.html

                    const pagination_link = form_el.parentElement.querySelectorAll('.pagination a.page-numbers')

                    if (pagination_link.length > 0) {
                        pagination_link.forEach(function (elem) {
                            elem.addEventListener("click", (e) => {
                                e.preventDefault()
                                formdata.pageNumber = parseInt(elem.innerHTML)
                                getNextPage(formdata);
                            });
                        });
                    }
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }


    // Loadmore button script
    if (loadmore_btn) {
        loadmore_btn.addEventListener('click', (e) => {
            let next_page = parseInt(loadmore_btn.dataset.curPage) + 1
            formdata.pageNumber = next_page
            fetch(simple_search_ajax_obj.ajax_url, {
                method: "POST",
                body: new URLSearchParams(formdata)
            })
                .then((response) => response.json())
                .then((data) => {
                    if (data) {
                        content_container.innerHTML += data.html
                        if (next_page == loadmore_btn.dataset.maxPage) {
                            loadmore_btn.classList.remove('active')
                        } else {
                            loadmore_btn.dataset.curPage = next_page
                        }
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
            e.preventDefault()
        })
    }

});