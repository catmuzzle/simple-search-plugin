<?php
/**
 * Template for search form
 */
?>

<form class="simple-search-form"
      action=""
      data-per-page="<?php echo sanitize_text_field($atts["element-count"]); ?>"
      data-types="<?php echo sanitize_text_field($atts["post-types"]); ?>"
>
    <input type="search" name="simple-search-query" placeholder="Search.." required />
    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('simple-search-nonce'); ?>">
    <input type="hidden" name="action" value="get_search_results_ajax">
</form>
